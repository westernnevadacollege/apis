<?php 
require_once __DIR__ . '/vendor/autoload.php';

loadEnvironmentConfig();

use WSDL\WSDLCreator;

require 'app/Routing.php';
$app = require 'app/AppServices.php';
$url = pieceOutUrl();

// Get some logging information together.
$log = array();
$log['service'] = $url['serviceCode'];
$log['ip'] = $_SERVER['REMOTE_ADDR'];

// Security
$allowedIps = require 'app/Config/AllowedIPs.php';
if ( ! in_array($_SERVER['REMOTE_ADDR'], $allowedIps))
{
    if (isWsdl($url))
    {
        $log['etc'] = 'wsdl';
    }
    $app['log']->alert('Access Denied', $log);
    
    send404();
    exit;
}

// Let's do work
if (isset($url['serviceCode']))
{
    // Attempt to find web service class
    $classMap = require 'app/Config/WebServices.php';
    
    if ( ! isset($classMap[$url['serviceCode']]))
    {
        $app['log']->notice('Service not found', $log);
        
        send404();
        exit;
    }
    
    $serviceClass = $classMap[$url['serviceCode']];
    $log['service'] = $serviceClass;
    
    $soapNameSpace = getHttpHost() . '/' . $url['serviceCode'];
    $serviceUrl = getBaseUrl('apis') . '/' . $url['serviceCode'] . '/';

    // WSDL Generation
    if (isWsdl($url))
    {        
        $wsdl = new WSDLCreator($serviceClass, $serviceUrl);
        $wsdl->setNamespace($soapNameSpace);
                
        $wsdl->renderWSDL();
        
        $app['log']->info('WSDL retrieved', $log);
    }
    
    // SOAP Server
    else
    {
        $serviceObject = new $serviceClass($app); 
        
        $server = new SoapServer($serviceUrl . 'wsdl/', array(
            'type_ns' => $soapNameSpace
        ));     
        $server->setObject($serviceObject);

        $server->handle();
        
        $app['log']->info('Service called', $log);
    }
}
exit;