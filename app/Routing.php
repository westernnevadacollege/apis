<?php

use Symfony\Component\HttpFoundation\Response;

/**
 * Returns the pieces of the url that are required by the application.
 * 
 * This function returns an array w/ keys 'serviceCode' and 'etc' for invertability.  If the URL 
 * structure were to change for some reason, simply make sure 'serviceCode' and 'etc' are populated 
 * and the rest of the application will be unaffected.
 * 
 * @return array
 */
function pieceOutUrl()
{ 
    $url = trim($_SERVER['REQUEST_URI'], '/');
    
    $url = explode('/', $url);
    
    if (isset($url[1], $url[2]))
    {
        return array('serviceCode' => $url[1], 'etc' => $url[2]);
    }
    elseif(isset($url[1]))
    {
        return array('serviceCode' => $url[1]);
    }
    else
    {
        return array();
    }
}

// ---------------------------------------------------------------------------------------------

/**
 * Was WSDL requested.
 * 
 * @param array $url
 * @return boolean
 */
function isWsdl($url)
{
    return isset($url['etc']) && $url['etc'] == 'wsdl';
}

// ---------------------------------------------------------------------------------------------

/**
 * Get the base url
 * 
 * @param string $add anything that you need to add on to the end of it.
 * @return string
 */
function getBaseUrl($add = null)
{
    $protocol = isSSL() ? 'https' : 'http';
    
    return $protocol . '://' . getHttpHost() . '/' . $add;
}

// ---------------------------------------------------------------------------------------------

/**
 * Get HTTP Host
 * 
 * @return string
 */
function getHttpHost()
{
    return $_SERVER['HTTP_HOST'];
}

// ---------------------------------------------------------------------------------------------

/**
 * Checks to see if the current connection is an SSL connection.
 * 
 * @return boolean
 */
function isSSL()
{
    return ( ! empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') 
            || $_SERVER['SERVER_PORT'] == 443;
}

// ---------------------------------------------------------------------------------------------

/**
 * Send a 404 response
 */
function send404()
{
    $response = new Response('', 404, array('Content-Type' => 'text/plain'));
    $response->send();    
}