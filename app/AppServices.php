<?php

use Illuminate\Container\Container;
use WesternNevadaCollege\Database;

$services = new Container();

/*
|--------------------------------------------------------------------------
| Database
|--------------------------------------------------------------------------
*/

$services->bind('db', function () 
{
    return new Database(
        'mysql', 
        $_ENV['mysql.host'], 
        'wncar_psis', 
        $_ENV['mysql.user'], 
        $_ENV['mysql.pass']
    );
});

/*
|--------------------------------------------------------------------------
| Logs
|--------------------------------------------------------------------------
*/

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$services->singleton('log', function ()
{    
    $logger = new Logger('GEN');
    $fileHandler = new StreamHandler(__DIR__ . '/../logs/api.log');
    $logger->pushHandler($fileHandler, Logger::DEBUG);
    
    return $logger;
});

/*
|--------------------------------------------------------------------------
| WNC Packages
|--------------------------------------------------------------------------
*/

$services->bind('holiday.schedule', function () use ($services)
{
    return new \WesternNevadaCollege\HolidaySchedule(array(
        'db.object' => $services['db'],
    ));
});

/*
|--------------------------------------------------------------------------
| Information Packages
|--------------------------------------------------------------------------
*/

$services->singleton('info.sr', function()
{
    return new \WesternNevadaCollege\PersonInfo\StudentRecords(array(
        'username' => $_ENV['mysql.user'], 
        'password' => $_ENV['mysql.pass'],
    ));
});

$services->singleton('info.sf', function()
{
    return new \WesternNevadaCollege\PersonInfo\StudentFinancials(array(
        'username' => $_ENV['mysql.user'], 
        'password' => $_ENV['mysql.pass'],
    ));
});

$services->singleton('info.fa', function()
{
    return new \WesternNevadaCollege\PersonInfo\FinancialAid(array(
        'username' => $_ENV['mysql.user'], 
        'password' => $_ENV['mysql.pass'],
    ));
});

$services->singleton('info.holds', function()
{
    return new \WesternNevadaCollege\PersonInfo\Holds(array(
        'username' => $_ENV['mysql.user'], 
        'password' => $_ENV['mysql.pass'],
    ));
});

// ---------------------------------------------------------------------------------------------
// ----------------------------------------------------------
// --------------------------

return $services;