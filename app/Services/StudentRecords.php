<?php

use Carbon\Carbon;

class StudentRecords extends Base
{
    /**
     * Application DI container.
     * 
     * @var Container
     */
    protected $app;
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Constructor
     * 
     * @param Container $app
     */
    public function __construct($app) 
    {
        parent::__construct($app);
        
        $this->app = $app;
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Is student active with WNC.
     * 
     * @param string $emplid
     * @return string $stat
     */
    public function isActive($emplid)
    {
        $q = "SELECT PROG_STATUS
            
            FROM wncar_psis.ACAD_PROG
            
            WHERE EMPLID = :emplid";
        
        $data = $this->db->select($q, compact('emplid'));
        
        if (count($data) < 1)
        {
            return 'XR';
        }
        
        foreach ($data as $row)
        {
            if ($row['PROG_STATUS'] == 'AC')
            {
                return 'Y';
            }
        }
        
        return 'N';
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Get student's active academic program.
     * 
     * @param string $emplid
     * @return string $prog
     */
    public function getAcadProg($emplid)
    {
        $q = "SELECT ACAD_PROG
            
            FROM wncar_psis.ACAD_PROG
            
            WHERE EMPLID = :emplid
            AND PROG_STATUS = 'AC' ";
        
        $data = $this->db->select($q, compact('emplid'));
        
        return isset($data[0]['ACAD_PROG']) ? $data[0]['ACAD_PROG'] : 'XR';
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Get future dated academic programs.
     * 
     * @param string $emplid
     * @return string $prog
     */
    public function getAcadProgFutrDtd($emplid)
    {
        $q = "SELECT A.ACAD_PROG \n"
                . "\n"
            . "FROM wncar_psis.ACAD_PROG_FULL A \n"
                . "\n"
            . "WHERE A.EFFDT = ( \n"
                . "SELECT MAX(A_ED.EFFDT) \n"
                . "\n"
                . "FROM wncar_psis.ACAD_PROG_FULL A_ED \n"
                . "\n"
                . "WHERE A.EMPLID = A_ED.EMPLID \n"
                . "AND A.ACAD_CAREER = A_ED.ACAD_CAREER \n"
                . "AND A.STDNT_CAR_NBR = A_ED.STDNT_CAR_NBR \n"
                . "AND A_ED.EFFDT > curdate() \n"
            . ") \n"
            . "AND A.EFFSEQ = ( \n"
                . "SELECT MAX(A_ES.EFFSEQ) \n"
                . "\n"
                . "FROM wncar_psis.ACAD_PROG_FULL A_ES \n"
                . "\n"
                . "WHERE A.EMPLID = A_ES.EMPLID \n"
                . "AND A.ACAD_CAREER = A_ES.ACAD_CAREER \n"
                . "AND A.STDNT_CAR_NBR = A_ES.STDNT_CAR_NBR \n"
                . "AND A.EFFDT = A_ES.EFFDT \n"
            . ")"
            . "AND A.EMPLID = :emplid \n"
            . "AND PROG_STATUS = 'AC' ";
        
        $data = $this->db->select($q, compact('emplid'));
        
        return isset($data[0]['ACAD_PROG']) ? $data[0]['ACAD_PROG'] : 'XR';
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Is student in class.
     * 
     * Find out if a student is in class during a time period.
     * 
     * @param string $id Student's ID.
     * @param string $start Start date time.  Format: "YYYY-MM-DD HH24:MM:SS".
     * @param string $end End date time.  Format: "YYYY-MM-DD HH24:MM:SS".
     * @return boolean $yesOrNo
     */
    public function inClass($id, $start, $end)
    {
        $schedule = $this->getSchedule($id, $start, $end);
        
        return count($schedule) > 0;
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Get student's schedule
     * 
     * Return student's schedule in an array.
     * 
     * @param string $id Student's ID.
     * @param string $start Start date time.  Format: "YYYY-MM-DD HH24:MM:SS".
     * @param string $end End date time.  Format: "YYYY-MM-DD HH24:MM:SS".
     * @return array $schedule Student's schedule.
     */
    public function getSchedule($id, $start, $end)
    {
        $start = new Carbon($start);
        $end = new Carbon($end);
        $startDt = $start->format('Y-m-d'); 
        $endDt = $end->format('Y-m-d');
        $startTm = $start->format('G:i:s');
        $endTm = $end->format('G:i:s');
        
        $q = "SELECT C.STRM, C.CLASS_NBR, C.SUBJECT, C.CATALOG_NBR, C.CLASS_SECTION, " 
            . "M.MEETING_TIME_START, M.MEETING_TIME_END, M.MON, M.TUES, M.WED, M.THURS, M.FRI, "
            . "M.SAT, M.SUN, M.START_DT, M.END_DT " 

            . "FROM wncar_psis.STDNT_ENRL E "

            . "JOIN wncar_psis.CLASS_TBL C "
            . "ON C.CLASS_NBR = E.CLASS_NBR "
            . "AND C.STRM = E.STRM "

            . "JOIN wncar_psis.CLASS_MTG_PAT M "
            . "ON `M`.`CRSE_ID` = `C`.`CRSE_ID` "
            . "AND `M`.`CRSE_OFFER_NBR` = `C`.`CRSE_OFFER_NBR` "
            . "AND `M`.`STRM` = `C`.`STRM` "
            . "AND `M`.`SESSION_CODE` = `C`.`SESSION_CODE` "
            . "AND `M`.`CLASS_SECTION` = `C`.`CLASS_SECTION` "

            . "WHERE E.EMPLID = :id "
                
            . "AND (M.START_DT <= :startDt " // Need to do this in order to capture entire range
            . "OR M.START_DT <= :endDt) "
                
            . "AND (M.END_DT >= :startDt "
            . "OR M.END_DT >= :endDt) "
                
            . "AND (M.MEETING_TIME_START <= :startTm "
            . "OR M.MEETING_TIME_START <= :endTm) "
                
            . "AND (M.MEETING_TIME_END >= :startTm "
            . "OR M.MEETING_TIME_END >= :endTm) "
                
            . "AND C.LOCATION <> 'WEB' "
            . "AND E.STDNT_ENRL_STATUS = 'E' ";
        
        // This query will only return classes who've started or ended; and classes that meet in 
        // between the input time.  It does NOT tell if the class is meeting on that particular day.
        $schedule = $this->db->select($q, compact('id', 'startDt', 'endDt', 'startTm', 'endTm'));

        foreach ($schedule as $key => &$class)
        {
            // Here we check if a class meets on a particular day and add information to array by &.
            if ( ! $this->checkMeetDays($class, $start, $end))
            {
                unset($schedule[$key]);
            }
        }
        
        return $schedule;
    }
    
    /**
     * Check meeting days.
     * 
     * Peoplesoft only returns class start/end date for date comparison. We have to process the 
     * class's "meeting days" information to truely determine if the class meets on a particular 
     * date.  Also, we need to check on the holiday schedule.  Will populate each class within array 
     * with an additional "MEETS_ON" element, containing date information on what days the class 
     * meets.
     * 
     * @param array &$class Class data from Peoplesoft.
     * @param string $start
     * @param string $end
     * @return boolean Whether the class meets on those days or not.
     */
    protected function checkMeetDays(&$class, $start, $end)
    {
        $ps2CarbonDayCodesTranslate = array(
            Carbon::MONDAY => 'MON', 
            Carbon::TUESDAY => 'TUES', 
            Carbon::WEDNESDAY => 'WED', 
            Carbon::THURSDAY => 'THURS', 
            Carbon::FRIDAY => 'FRI', 
            Carbon::SATURDAY => 'SAT', 
            Carbon::SUNDAY => 'SUN',
        );
        $daysBetween = $end->diffInDays($start);
        $scrollDate = clone $start;
        
        for ($i = 0; $i <= $daysBetween; $i++)
        {   
            if ($this->app['holiday.schedule']->isHoliday($scrollDate, $start, $end))
            {
                continue;
            }
                
            $psDayCode = $ps2CarbonDayCodesTranslate[$scrollDate->dayOfWeek];

            if ($class[$psDayCode] == 'Y')
            {                             
                $class['MEETS_ON'][] = sprintf('%s, %s, %s-%s', 
                                       $psDayCode, 
                                       $scrollDate->format('Y-m-d'), 
                                       $class['MEETING_TIME_START'], 
                                       $class['MEETING_TIME_END']);
            }

            $scrollDate = $scrollDate->addDay();
        }

        return isset($class['MEETS_ON']);
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Get a students FERPA disclosure
     * 
     * @param string $emplid
     * @return string $disclosure
     */
    public function getFerpaDisclosure($emplid)
    {
        return $this->app['info.sr']->getFerpaDisclosure($emplid);
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Check if a student is in PTK
     * 
     * @param string $emplid
     * @return boolean $yesOrNo
     */
    public function inPTK($emplid)
    {
        return $this->app['info.sr']->inPTK($emplid);
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Get a person's PTK chapter.
     * 
     * @param string $emplid
     * @return string $chapter
     */
    public function getPTKChapter($emplid)
    {
        return $this->app['info.sr']->getPTKChapter($emplid) ?: 'XR';
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Get a person's awarded degrees
     * 
     * @param string $emplid
     * @return array $degrees
     */
    public function getDegrees($emplid)
    {
        return $this->app['info.sr']->getDegrees($emplid);
    }
}
