<?php

class Holds extends Base
{
    /**
     * Check if student has a past due balance hold
     * 
     * @param string $emplid
     * @return boolean $yesOrNo
     */
    public function hasPastDueBalance($emplid)
    {
        return $this->app['info.holds']->hasPastDueBalance($emplid);
    }
}