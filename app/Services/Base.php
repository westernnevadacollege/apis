<?php

class Base 
{
    /**
     * Database
     * 
     * @var \WesternNevadaCollege\Database 
     */
    protected $db;
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Constructor
     * 
     * @param $app
     */
    public function __construct($app) 
    {
        $this->db = $app['db'];
        $this->app = $app;
    }
}