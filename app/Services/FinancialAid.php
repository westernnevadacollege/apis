<?php

class FinancialAid extends Base
{  
    /**
     * Check dependency status
     *
     * @param string $emplid
     * @param string $aidYear
     * @return string $stat
     */
    public function getDependencyStatus($emplid, $aidYear)
    {        
        $data = $this->db->select("
            SELECT DEPNDNCY_STAT
            
            FROM wncar_psis.ISIR_STUDENT
            
            WHERE EMPLID = :emplid
            AND AID_YEAR = :aidYear
            
        ", compact('emplid', 'aidYear'));
        
        return isset($data[0]['DEPNDNCY_STAT']) ? $data[0]['DEPNDNCY_STAT'] : 'XR';
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Check verification status
     *
     * @param string $emplid
     * @param string $aidYear
     * @return string $stat
     */
    public function getVerificationStatus($emplid, $aidYear)
    {
        $data = $this->db->select("
            SELECT VERIF_TRK_FLG 
            
            FROM wncar_psis.ISIR_CONTROL
            
            WHERE EMPLID = :emplid
            AND AID_YEAR = :aidYear
            
        ", compact('emplid', 'aidYear'));
        
        return isset($data[0]['VERIF_TRK_FLG']) ? $data[0]['VERIF_TRK_FLG'] : 'XR';
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Check if student has submitted an ISIR
     * 
     * @param string $emplid
     * @param string $aidYear
     * @return boolean $yesOrNo
     */
    public function isISIRSubmitted($emplid, $aidYear)
    {
        return count($this->app['info.fa']->getISIR($emplid, $aidYear)) > 0;
    }
    
    // ---------------------------------------------------------------------------------------------
	
    /**
     * DRY method.  Switch through "did student 'link' to IRS using their data retrieval tool" 
     * parent or student fields.
     * 
     * @param string $userId
     * @param string $aidYear
     * @param string $field DB field name.
     * @return string
     */
	protected function taxesLinkedToIrsSwitch($userId, $aidYear, $field)
	{
		$q = "SELECT A.{$field} "

            . "FROM wncar_psis.ISIR_CONTROL A "

            . "WHERE A.EMPLID = :userId "
            . "AND A.AID_YEAR = :aidYear ";
        
        $data = $this->db->select($q, compact('userId', 'aidYear'));
				
		if (count($data) < 1) {
			return '';
		}
		
		return $data[0][$field] === '02' ? 'Y' : 'N';
	}
    
    // ---------------------------------------------------------------------------------------------
	
	/**
     * Check if student linked their parent's taxes to IRS using data retrieval tool.
     * 
     * @param string $emplid
     * @param string $aidYear
     * @return string $code Y, N, or blank
     */
	public function parentTaxesLinkedToIrs($emplid, $aidYear)
	{
		return $this->taxesLinkedToIrsSwitch($emplid, $aidYear, 'SFA_PAR_IRS_REQUST');
	}
    
    // ---------------------------------------------------------------------------------------------
	
	/**
     * Check if student linked their own taxes to IRS using data retrieval tool.
     * 
     * @param string $emplid
     * @param string $aidYear
     * @return string $code Y, N, or blank
     */
	public function studentTaxesLinkedToIrs($emplid, $aidYear)
	{
		return $this->taxesLinkedToIrsSwitch($emplid, $aidYear, 'SFA_STU_IRS_REQUST');
	}
}