<?php 

use WesternNevadaCollege\Academic\Term;

class StudentFinancials extends Base
{
    /**
     * Check if a student has paid a grad fee.
     * 
     * @param string $emplid
     * @param string $termType FALL|SPRING|SUMMBER
     * @param int $year
     * @return boolean $yesOrNo
     */
    public function hasPaidGradFee($emplid, $termType, $year)
    {
        $termTypeXlate = constant('WesternNevadaCollege\Academic\Term::' . strtoupper($termType));
        
        if (null === $termTypeXlate) {
            throw new \LogicException('Unknown code: '. $termType);
        }

        return $this->app['info.sf']->hasPaidGradFee($emplid, Term::makeCode($termTypeXlate, $year));
    }
}