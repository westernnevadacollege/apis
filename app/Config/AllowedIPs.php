<?php return array(
    '134.197.173.111',  // WNC internal
    '134.197.186.120',  // Bilbo (So it can retrieve a WSDL from itself)
    '63.240.201.131',   // Onbase
    '127.0.0.1',        // Localhost
);